package me.xethh.utils.authUtils.authentication.impl.common;

public class AccessUnit implements me.xethh.utils.authUtils.authentication.AccessUnit {
    private String accessName,accessToken;

    public AccessUnit(String accessName, String accessToken) {
        this.accessName = accessName;
        this.accessToken = accessToken;
    }

    @Override
    public String accessName() {
        return accessName;
    }

    @Override
    public String accessToken() {
        return accessToken;
    }

    @Override
    public String toString() {
        return "AccessUnit{" +
                "accessName='" + accessName + '\'' +
                ", accessToken='" + accessToken + '\'' +
                '}';
    }
}
