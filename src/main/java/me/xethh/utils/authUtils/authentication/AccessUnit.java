package me.xethh.utils.authUtils.authentication;

public interface AccessUnit {
    String accessName();
    String accessToken();
}
