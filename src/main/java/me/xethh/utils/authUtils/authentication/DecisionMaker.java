package me.xethh.utils.authUtils.authentication;

public interface DecisionMaker<X extends AccessUnit> {
    Boolean isValid(X unit);
}
