package me.xethh.utils.authUtils.authentication;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.function.Supplier;

public class Encoder {
    public Encoder() {
        this.bCryptPasswordEncoder = newBCryptEncoder();
    }

    public BCryptPasswordEncoder getbCryptPasswordEncoder() {
        return bCryptPasswordEncoder;
    }

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private static Encoder _encoder;
    private static Encoder anyEncoder(){
        if(_encoder==null){
            _encoder = new Encoder();
        }
        return _encoder;
    }
    public static BCryptPasswordEncoder newBCryptEncoder(){
        return new BCryptPasswordEncoder();
    }
    public static BCryptPasswordEncoder newBCryptEncoder(Supplier<BCryptPasswordEncoder> encoderProvider){
        return encoderProvider.get();
    }
    public String encode(String token){
        return bCryptPasswordEncoder.encode(token);
    }
    public static String staticEncode(String token){
        return anyEncoder().encode(token);
    }
    public boolean match(String token, String encoded){
        return bCryptPasswordEncoder.matches(token, encoded);
    }
    public static boolean staticMatch(String token, String encoded){
        return anyEncoder().match(token,encoded);
    }

    public static void main(String[] args){
        switch (args.length){
            case 1: System.out.println(String.format("Encoding[%s] to [%s]", args[0], anyEncoder().encode(args[0])));
            case 2: System.out.println(String.format("Matching raw password[%s] and hashed code[%s], result %b",args[0], args[1],anyEncoder().match(args[0],args[1])));
            default:
                throw new RuntimeException("Operation not supported");
        }
    }
}
