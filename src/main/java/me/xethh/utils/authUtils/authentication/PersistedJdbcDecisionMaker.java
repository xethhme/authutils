package me.xethh.utils.authUtils.authentication;

import me.xethh.utils.JDBCProvider.PersistedJDBCProvider;
import me.xethh.utils.authUtils.authentication.impl.common.AccessUnit;

import java.sql.Connection;

public abstract class PersistedJdbcDecisionMaker<X extends AccessUnit> implements JdbcDecisionMaker<X> {
    private PersistedJDBCProvider provider;
    private Encoder encoder;

    public PersistedJdbcDecisionMaker(PersistedJDBCProvider provider) {
        this.provider = provider;
        encoder = encoder();
    }

    @Override
    public Connection conn() {
        int start = 3;
        RuntimeException lastException = null;
        while (start>0){
            try {
                return provider.getConnection();
            }
            catch (Exception ex){
                start--;
                lastException = new RuntimeException(ex);
            }
        }
        throw lastException;
    }

    @Override
    public Encoder encoder() {
        return new Encoder();
    }
}
