package me.xethh.utils.authUtils.authentication;

import me.xethh.utils.authUtils.authentication.impl.common.AccessUnit;

import java.sql.Connection;

public interface JdbcDecisionMaker<X extends AccessUnit> extends DecisionMaker<X> {
    String select(Connection connection, String accessName);

    Connection conn();

    Encoder encoder();

    @Override
    default Boolean isValid(X unit) {
        String hashed = select(conn(), unit.accessName());
        return hashed==null?false:encoder().match(unit.accessToken(),hashed);
    }
}
