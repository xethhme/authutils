package me.xethh.utils.authUtils.authentication.test;

import me.xethh.utils.JDBCProvider.JDBCDrivers;
import me.xethh.utils.JDBCProvider.JDBCProviderFactory;
import me.xethh.utils.JDBCProvider.PersistedJDBCProvider;
import me.xethh.utils.authUtils.authentication.Encoder;
import me.xethh.utils.authUtils.authentication.PersistedJdbcDecisionMaker;
import me.xethh.utils.authUtils.authentication.impl.common.AccessUnit;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class TestCase {
    @BeforeClass
    public static void init() throws SQLException {
        PersistedJDBCProvider provider = JDBCProviderFactory.createPersistedProvider(JDBCDrivers.Mysql_New.driver(), "jdbc:mysql://localhost/authUtilsDB", "authUtilsDB", "authUtilsDB");

        Connection conn = provider.getConnection();
        conn.createStatement().execute("drop table if exists auth");
        conn.createStatement().execute("" +
                "create table auth(\n" +
                "\tusername varchar(64) not null,\n" +
                "    password varchar(512) not null,\n" +
                "    primary key(username)\n" +
                ")"
        );
        conn.createStatement().execute(String.format("insert into auth(username,password) values ('xeth','%s')", Encoder.newBCryptEncoder().encode("Abcd1234")));
    }

    public static class TempMaker extends PersistedJdbcDecisionMaker<AccessUnit>{

        public TempMaker(PersistedJDBCProvider provider) {
            super(provider);
        }

        @Override
        public String select(Connection connection, String accessName) {
            try {
                PreparedStatement stmt = connection.prepareStatement("select password from auth where username=?");
                stmt.setString(1,accessName);
                ResultSet rs = stmt.executeQuery();
                return rs.next()?rs.getString("password"):null;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    public void test(){
        PersistedJDBCProvider provider = JDBCProviderFactory.createPersistedProvider(JDBCDrivers.Mysql_New.driver(), "jdbc:mysql://localhost/authUtilsDB", "authUtilsDB", "authUtilsDB");
        TempMaker maker = new TempMaker(provider);
        assertEquals(true,maker.isValid(new AccessUnit("xeth","Abcd1234")));
        assertEquals(false,maker.isValid(new AccessUnit("xeth","Addd")));
        assertEquals(false,maker.isValid(new AccessUnit("ddd","Abcd1234")));
    }
}
